package com.zzx.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class SecurityController {

    @RequestMapping("/add")
    @PreAuthorize("hasAuthority('add')")
    public String add() {
        System.out.println("add....");
        return null;
    }

    @RequestMapping("/update")
    @PreAuthorize("hasAuthority('update')")
    public String update() {
        System.out.println("update....");
        return null;
    }

    @RequestMapping("/delete")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String delete() {
        System.out.println("delete....");
        return null;
    }

}
