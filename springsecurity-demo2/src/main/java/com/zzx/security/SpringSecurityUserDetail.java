package com.zzx.security;

import com.zzx.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.List;

public class SpringSecurityUserDetail implements UserDetailsService {

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //从数据库中查询User对象
        User user = findByUserName(username);
        //判断是否为Null
        if (user == null) {
            return null;
        }
        //获取用户权限列表
        List<GrantedAuthority> list = new ArrayList<>();
        list.add(new SimpleGrantedAuthority("add"));
        list.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        //返回UserDetail对象
        org.springframework.security.core.userdetails.User userDetail =
                new org.springframework.security.core.userdetails.User(username, user.getPassword(), list);
        return userDetail;
    }


    private User findByUserName(String username) {
        if ("admin".equals(username)) {
            User user = new User();
            user.setUsername(username);
            user.setPassword(encoder.encode("123456"));
            return user;
        }
        return null;
    }
}
