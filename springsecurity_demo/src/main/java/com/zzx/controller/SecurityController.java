package com.zzx.controller;


import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RequestMapping("/hello")
public class SecurityController {

    @RequestMapping("/add")
    @PreAuthorize("hasAuthority('add')") //用户必须拥有add权限才能调用当前方法
    public String add() {
        System.out.println("add....");
        return null;
    }

    @RequestMapping("/delete")
    @PreAuthorize("hasRole('update')")
    public String delete() {
        System.out.println("delete...");
        return null;
    }

}
