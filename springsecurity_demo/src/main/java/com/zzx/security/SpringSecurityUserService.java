package com.zzx.security;

import com.zzx.pojo.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


import java.util.ArrayList;
import java.util.List;


public class SpringSecurityUserService implements UserDetailsService {
    
    private BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //根据用户名查询数据库，获得User对象
        User user = findUserByUsername(username);
        //判断是否为空
        if (user == null) {
            return null;
        }
        //获取用户名的权限列表
        List<GrantedAuthority> list = new ArrayList<>();
        list.add(new SimpleGrantedAuthority("add"));
        list.add(new SimpleGrantedAuthority("delete"));
        list.add(new SimpleGrantedAuthority("ROLE_ADMIN"));

        //将用户的账号，密码，以及权限列表封装到UserDetail里的User对象
        org.springframework.security.core.userdetails.User userDetail =
                new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), list);
        return userDetail;
    }

    public User findUserByUsername(String username) {
        if ("admin".equals(username)) {
            User user = new User();
            user.setUsername(username);
            user.setPassword(bcrypt.encode("123456"));
            return user;
        }
        return null;
    }

}
