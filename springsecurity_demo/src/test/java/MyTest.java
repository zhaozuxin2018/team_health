import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-security.xml")
public class MyTest {

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Test
    public void fun1() {
        System.out.println(encoder.encode("123456"));
    }

}
