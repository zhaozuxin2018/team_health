package com.zzx.quartz;

import com.zzx.dao.OrderSettingDao;
import com.zzx.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

/**
 * 定时清理历史数据
 */
public class CleanOrderSetting {

    @Autowired
    private OrderSettingDao orderSettingDao;

    public void cleanOrderSettingDao() throws Exception {

        System.out.println("凌晨两点到 删除文件");
        //获取本月最后一天
        Date monthEnd = DateUtils.getMonthEnd();
        String orderDate = DateUtils.parseDate2String(monthEnd, "yyyy-MM-dd");
          /*      <!-- ======而对于已经过去的历史数据可以定时来进行清理，
        例如每月最后一天凌晨2点执行一次清理任务===================-->*/
        orderSettingDao.cleanOrderSettingDao(orderDate);
    }
}
