package com.zzx.jobs;

import com.zzx.constant.RedisConstants;
import com.zzx.utils.QiniuUtils;
import org.springframework.beans.factory.annotation.Autowired;
import redis.clients.jedis.JedisPool;

import java.util.Set;

public class ClearImgJob {

    @Autowired
    private JedisPool jedisPool;

    //清理图片
    public void clearImg() {
        System.out.println("clear image...");
        //计算set的差值
        Set<String> sets = jedisPool.getResource().sdiff(RedisConstants.PIC_UPLOAD_QINIU, RedisConstants.PIC_UPLOAD_DB);
        //遍历，并进行删除
        for (String imgName : sets) {
            //删除七牛云上的图片
            QiniuUtils.deleteFileFromQiniu(imgName);
            //删除redis里的图片
            jedisPool.getResource().srem(RedisConstants.PIC_UPLOAD_QINIU, imgName);
        }
    }

}
