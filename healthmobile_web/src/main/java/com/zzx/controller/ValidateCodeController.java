package com.zzx.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.aliyuncs.exceptions.ClientException;
import com.zzx.constant.MessageConstant;
import com.zzx.constant.RedisConstants;
import com.zzx.constant.RedisMessageConstant;
import com.zzx.entity.Result;
import com.zzx.service.ValidateCodeService;
import com.zzx.utils.SMSUtils;
import com.zzx.utils.ValidateCodeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.JedisPool;

@RestController
@RequestMapping("/validateCode")
public class ValidateCodeController {

    @Reference
    private ValidateCodeService validateCodeService;

    @Autowired
    private JedisPool jedisPool;

    /**
     * 预约发送验证码
     *
     * @param telephone
     * @return
     */
    @RequestMapping("/send4Order")
    public Result send4Order(String telephone) {
        //获取4位随机验证码
        Integer code = ValidateCodeUtils.generateValidateCode(4);
        try {
            //调用发送短信的方法
            SMSUtils.sendShortMessage(SMSUtils.VALIDATE_CODE, telephone, code + "");
            //将验证码存储到Redis
            jedisPool.getResource().setex(telephone + RedisMessageConstant.SENDTYPE_ORDER, 5 * 60, code.toString());
            return new Result(true, MessageConstant.SEND_VALIDATECODE_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.SEND_VALIDATECODE_FAIL);
        }
    }

    /**
     * 登录发送验证码
     */
    @RequestMapping("/send4Login")
    public Result send4Login(String telephone) {
        //先判断该号码是否已经发送过验证码了
        String result = jedisPool.getResource().get(telephone + RedisMessageConstant.SENDTYPE_LOGIN);
        if (result != null && result.length() > 0) {
            //已经发送过验证码了
            return new Result(false, MessageConstant.ALREADY_SEND_MESSAGE);
        }
        //没发过的，就发送一次验证码
        //获取四位数验证码
        Integer validateCode = ValidateCodeUtils.generateValidateCode(4);
        try {
            //发送验证码
            SMSUtils.sendShortMessage(SMSUtils.VALIDATE_CODE, telephone, validateCode.toString());
            //存储到reids，并保存5分钟
            jedisPool.getResource().setex(telephone + RedisMessageConstant.SENDTYPE_LOGIN, 5 * 60, validateCode.toString());
            return new Result(true, MessageConstant.SEND_VALIDATECODE_SUCCESS);
        } catch (ClientException e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.SEND_VALIDATECODE_FAIL);
        }
    }


}
