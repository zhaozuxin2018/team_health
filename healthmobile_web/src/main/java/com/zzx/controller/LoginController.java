package com.zzx.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zzx.constant.MessageConstant;
import com.zzx.constant.RedisMessageConstant;
import com.zzx.entity.Result;
import com.zzx.pojo.Member;
import com.zzx.service.LoginService;
import com.zzx.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.JedisPool;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping("/login")
public class LoginController {


    @Autowired
    private JedisPool jedisPool;

    @Reference
    private MemberService memberService;

    @RequestMapping("/loginAction")
    public Result loginAction(@RequestBody Map map, HttpServletResponse resp) {
        //获取电话
        String telephone = (String) map.get("telephone");
        //获取验证码
        String validateCode = (String) map.get("validateCode");
        //获取jeds中验证码
        String result = jedisPool.getResource().get(telephone + RedisMessageConstant.SENDTYPE_LOGIN);
        if (result == null || !validateCode.equals(result)) {
            return new Result(false, MessageConstant.VALIDATECODE_ERROR);
        }
        try {
            //验证码相等了，再看账号是否已经被注册过会员
            int count = memberService.findCountByPhoneNumber(telephone);
            if (count > 0) {
                //注册过了会员，直接登录
            } else {
                //不是会员就快速注册成一个会员
                Member member = new Member(null, null, null, telephone, new Date());
                memberService.add(member);
            }
            //发送一个Cookie
            Cookie cookie = new Cookie("telephone_cookie", telephone);
            cookie.setPath("/");
            cookie.setMaxAge(60 * 60 * 24 * 30);
            resp.addCookie(cookie);
            return new Result(true, MessageConstant.LOGIN_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.LOGIN_FAIL);
        }

    }

}
