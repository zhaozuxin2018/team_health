package com.zzx.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.zzx.constant.MessageConstant;
import com.zzx.entity.Result;
import com.zzx.pojo.Setmeal;
import com.zzx.service.SetmealService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/setmeal")
public class SetmealController {

    @Reference
    private SetmealService setmealService;

    /**
     * 获取所有套餐
     *
     * @return
     */
    @RequestMapping("/getSetmeal")
    public Result getSetmeal() {

        try {
            String list = setmealService.getSetmeal();
            List<Setmeal> setmeals = JSON.parseArray(list, Setmeal.class);
            return new Result(true, MessageConstant.QUERY_SETMEALLIST_SUCCESS, setmeals);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_SETMEALLIST_FAIL);
        }
    }

    /**
     * 查询封装后的套餐（包含相应的检查组和检查项）
     */
    @RequestMapping("/findById")
    public Result findById(String id) {
        try {
            String setmeal = setmealService.findById(id);
            Setmeal setmeal1 = JSON.parseObject(setmeal, Setmeal.class);
            return new Result(true, MessageConstant.QUERY_SETMEAL_SUCCESS, setmeal1);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_SETMEAL_FAIL);
        }
    }


}
