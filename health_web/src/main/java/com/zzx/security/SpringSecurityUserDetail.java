package com.zzx.security;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zzx.pojo.Permission;
import com.zzx.pojo.Role;
import com.zzx.pojo.User;
import com.zzx.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

//@Component("springSecurityUserDetail")
public class SpringSecurityUserDetail implements UserDetailsService {

    @Reference
    private UserService userService;

    @Autowired
    private BCryptPasswordEncoder encoder;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        //从数据库中查询User对象
        User user = userService.findUserByUsername(username);
        //如果user为空就返回为空
        if (user == null) {
            return null;
        }

        List<GrantedAuthority> list = new ArrayList<>();
        //授权
        //根据用户id查询封装好了roles
        Set<Role> roles = userService.findRoleByUserId(user.getId());
        for (Role role : roles) {
            list.add(new SimpleGrantedAuthority(role.getKeyword()));
            Set<Permission> permissions = role.getPermissions();
            for (Permission permission : permissions) {
                list.add(new SimpleGrantedAuthority(permission.getKeyword()));
            }
        }
        return new org.springframework.security.core.userdetails.User(username, user.getPassword(), list);
    }
}
