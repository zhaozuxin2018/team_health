package com.zzx.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zzx.constant.MessageConstant;
import com.zzx.entity.Result;
import com.zzx.service.BusinessService;
import com.zzx.service.ReportService;
import net.sf.jxls.transformer.XLSTransformer;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.lang.reflect.Array;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/report")
public class ReportController {

    @Reference
    private ReportService reportService;

    @Reference
    private BusinessService businessService;

    /**
     * 查询会员数量
     *
     * @return
     */
    @RequestMapping("/getMemberReport")
    public Result getMemberReport() {
        try {
            Map<String, Object> map = reportService.getMemberReport();
            return new Result(true, MessageConstant.GET_MEMBER_NUMBER_REPORT_SUCCESS, map);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_MEMBER_NUMBER_REPORT_FAIL);
        }
    }

    /**
     * 根据日期展示会员数量折线图
     * @param array
     * @return
     */
    @RequestMapping("/getRangeMemberReport")
    public Result getRangeMemberReport(@RequestBody String[] array){
        try {
            Map<String, Object> map = reportService.getRangeMemberReport(array);
            return new Result(true, MessageConstant.GET_MEMBER_NUMBER_REPORT_SUCCESS, map);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_MEMBER_NUMBER_REPORT_FAIL);
        }
    }

    /**
     * 获取套餐预约占比
     *
     * @return
     */
    @RequestMapping("/getSetmealReport")
    public Result getSetmealReport() {
        try {
            Map<String, Object> map = reportService.getSetmealReport();
            return new Result(true, MessageConstant.GET_SETMEAL_COUNT_REPORT_SUCCESS, map);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_SETMEAL_COUNT_REPORT_FAIL);
        }
    }

    /**
     * 获取会员男女数量的占比
     *
     * @return
     */
    @RequestMapping(value = "/getMemberNumber", method = RequestMethod.GET)
    public Result getMemberNumber() {
        try {
            Map<String, Object> map = reportService.getMemberNumber();
            System.out.println(map);
            return new Result(true, MessageConstant.GET_MEMBER_NUMBER_REPORT_SUCCESS, map);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_MEMBER_NUMBER_REPORT_FAIL);
        }
    }

    //年龄占比
    @RequestMapping("/getBirthday")
    public Result getBirthday() throws Exception {
        //查询年龄
        Map<String, Object> setmealCount = reportService.setmealCount();
        return new Result(true,MessageConstant.EDIT_MEMBER_SUCCESS,setmealCount);
    }

    /**
     * 导出报表
     */
    @RequestMapping("/exportBusinessReport")
    public void getExportBusinessReport(HttpServletRequest request, HttpServletResponse response) {
        try {
            //获取预置模真实路径
            String filePath = request.getSession().getServletContext().getRealPath("template") + File.separator + "report.xlsx";
            //获取XSSFWORK
            XSSFWorkbook xssfWorkbook = new XSSFWorkbook(new FileInputStream(new File(filePath)));
            //获取Map数据
            Map<String, Object> map = businessService.getBusinessReportData();
            //通过xls工具将map数据填充到xssfWorkBook里面
            XLSTransformer xlsTransformer = new XLSTransformer();
            xlsTransformer.transformWorkbook(xssfWorkbook, map);
            //获取输出流
            OutputStream outputStream = response.getOutputStream();
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setHeader("content-Disposition", "attachment;filename=report.xlsx");

            xssfWorkbook.write(outputStream);

            //关闭资源
            outputStream.flush();
            outputStream.close();
            xssfWorkbook.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
