package com.zzx.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zzx.entity.Result;
import com.zzx.service.HelloService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class HelloController {

    @Reference
    private HelloService helloService;


    @RequestMapping("/sayHello")
    public Result sayHello(String name) {
        String str = helloService.sayHello(name);
        System.out.println(str);
        return null;
    }

}
