package com.zzx.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zzx.constant.MessageConstant;
import com.zzx.entity.PageResult;
import com.zzx.entity.QueryPageBean;
import com.zzx.entity.Result;
import com.zzx.pojo.CheckGroup;
import com.zzx.pojo.User;
import com.zzx.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Reference
    private UserService userService;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @RequestMapping("/getUserName")
    public Result getUserName() {
        try {
            //获取springSecuriy上下文对象
//            System.out.println(SecurityContextHolder.getContext().getAuthentication().getPrincipal());
            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return new Result(true, MessageConstant.GET_USERNAME_SUCCESS, user.getUsername());
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_USERNAME_FAIL);
        }
    }

    /**
     * 分页查询
     *
     * @param queryPageBean
     * @return
     */
    @RequestMapping("/findPage")
    private PageResult findPage(@RequestBody QueryPageBean queryPageBean) {
        return userService.findPage(queryPageBean);
    }

    /**
     * 添加用户
     */
    @RequestMapping("/add")
    private Result add(@RequestBody User user, Integer[] roleIds) {
        try {
            user.setPassword(encoder.encode(user.getPassword()));
            //调用业务
            userService.add(user, roleIds);
            return new Result(true, MessageConstant.ADD_CHECKGROUP_SUCCESS);
        } catch (Exception e) {
            return new Result(false, MessageConstant.ADD_CHECKGROUP_FAIL);
        }
    }

    /**
     * 查询用户
     */
    @RequestMapping("/findById")
    public Result findById(Integer id) {
        //调用业务，查询检查组
        User user = userService.findById(id);
        return new Result(true, MessageConstant.QUERY_CHECKGROUP_SUCCESS, user);
    }

    /**
     * 根据检查组id查询与之关联的检查项id们
     */
    @RequestMapping("/findRoleIdsByUserId")
    public List<Integer> findRoleIdsByUserId(Integer id) {

        List<Integer> ids = userService.findRoleIdsByUserId(id);
        return ids;
    }

    /**
     * 编辑检查组
     */
    @RequestMapping("/edit")
    public Result edit(@RequestBody User user, Integer[] roleIds) {
        try {
            //用户密码加盐
            user.setPassword(encoder.encode(user.getPassword()));
            userService.edit(user, roleIds);
            return new Result(true, "编辑用户成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "编辑用户失败");
        }
    }

    /**
     * 删除检查组
     */
    @RequestMapping("/delete")
    public Result delete(Integer id) {
        try {
            userService.delete(id);
            return new Result(true, "删除用户成功");
        } catch (RuntimeException e) {
            e.printStackTrace();
            return new Result(false, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "删除用户失败");
        }
    }


}
