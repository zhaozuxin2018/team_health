package com.zzx.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zzx.constant.MessageConstant;
import com.zzx.entity.PageResult;
import com.zzx.entity.QueryPageBean;
import com.zzx.entity.Result;
import com.zzx.pojo.CheckItem;
import com.zzx.pojo.Menu;
import com.zzx.service.MenuService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/menu")
public class MenuController {

    @Reference
    private MenuService menuService;

    /**
     * 查询菜单数据(需要根据登录用户, 动态获取菜单数据进行返回, 让前端页面进行显示)
                    *
                    * @return 菜单数据
                    */
            @RequestMapping(value = "/findMenuInfo", method = RequestMethod.POST)
            public Result findMenuInfo() {
                try {
                    // 获取用户名
                    String username = SecurityContextHolder.getContext().getAuthentication().getName();
                    //查询菜单信息
                    List<Menu> list = menuService.findMenuInfo(username);

                    return new Result(true, MessageConstant.GET_MENU_SUCCESS, list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "查询图标列表失败!");
        }
    }

    /**
     * 查询图标列表
     *
     * @return
     */
    @RequestMapping(value = "/findIcos", method = RequestMethod.GET)
    public Result findIcos() {
        try {
            //获取图标列表
            List<Map<String, Object>> list = menuService.findIcos();
            return new Result(true, MessageConstant.GET_MENU_SUCCESS, list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_MENU_FAIL);
        }
    }

    /**
     * 查询父级菜单IDS
     *
     * @return
     */
    @RequestMapping(value = "/findPids", method = RequestMethod.GET)
    public Result findPids() {
        try {
            //获取图标列表
            List<Map<String, Object>> list = menuService.findPids();
            return new Result(true, MessageConstant.GET_MENU_SUCCESS, list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_MENU_FAIL);
        }
    }


    /**
     * 添加菜单
     *
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result add(@RequestBody Menu menu) {
        try {
            System.out.println(menu);
            //添加菜单
            menuService.add(menu);
            return new Result(true, MessageConstant.ADD_MENU_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ADD_MENU_FAIL);
        }
    }

    /**
     * 根据级别1回显相应的路径
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/showPath", method = RequestMethod.POST)
    public Result showPath(@RequestBody Map map) {
        try {
            String level = map.get("level").toString();
            String parentMenuId = "";
            if ("1".equals(level)) {
                parentMenuId = null;
            } else {
                parentMenuId = map.get("parentMenuId").toString();
            }


            String path = menuService.showPath(Integer.parseInt(level), parentMenuId);
            return new Result(true, "回显路径成功!", path);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "回显路径失败!");
        }
    }

    /**
     * 根据id查询检查项
     */
    @RequestMapping(value = "/findById", method = RequestMethod.GET)
    public Result findById(Integer id) {
        try {
            //调用业务
            Menu menu = menuService.findById(id);
            return new Result(true, MessageConstant.QUERY_CHECKITEM_SUCCESS, menu);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_CHECKITEM_FAIL);
        }
    }

    /**
     * 获得分页数据对象
     *
     * @param queryPageBean
     * @return
     */
    @RequestMapping("/findPage")
    @PreAuthorize("hasAuthority('CHECKITEM_QUERY')")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean) {
        //调用业务获得分页数据对象
        PageResult pageResult = menuService.findPage(queryPageBean);
        return pageResult;
    }

    /**
     * 编辑检查项
     */
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result edit(@RequestBody Menu menu) {
        try {
            //调用业务，编辑检查项
            menuService.edit(menu);
            return new Result(true, MessageConstant.EDIT_CHECKITEM_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.EDIT_CHECKITEM_FAIL);
        }
    }

    /**
     * 根据id删除检查项
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Result delete(Integer id) {
        try {
            menuService.delete(id);
            return new Result(true, MessageConstant.DELETE_CHECKITEM_SUCCESS);
        } catch (RuntimeException e) {
            e.printStackTrace();
            return new Result(false, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.DELETE_CHECKITEM_FAIL);
        }
    }


}
