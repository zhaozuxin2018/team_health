package com.zzx.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zzx.constant.MessageConstant;
import com.zzx.constant.RedisConstants;
import com.zzx.entity.PageResult;
import com.zzx.entity.QueryPageBean;
import com.zzx.entity.Result;
import com.zzx.pojo.Setmeal;
import com.zzx.service.SetmealService;
import com.zzx.utils.QiniuUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import redis.clients.jedis.JedisPool;

import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("/setmeal")
public class SetmealController {

    @Reference
    private SetmealService setmealService;

    @Autowired
    private JedisPool jedisPool;

    /**
     * 分页查询
     *
     * @param queryPageBean
     * @return
     */
    @RequestMapping("/findPage")
    private PageResult findPage(@RequestBody QueryPageBean queryPageBean) {
        System.out.println(queryPageBean);
        return setmealService.findPage(queryPageBean);
    }

    /**
     * 图片上传
     */
    @RequestMapping("/upload")
    private Result upload(@RequestBody MultipartFile imgFile) {
        try {
            //获取图片名称
            String oriName = imgFile.getOriginalFilename();
            //获取后缀
            String suffix = oriName.substring(oriName.lastIndexOf("."));
            //获取随机名
            String filename = UUID.randomUUID().toString().replace("-", "") + suffix;
            //调用七牛云工具类，上传至七牛云
            QiniuUtils.upload2Qiniu(imgFile.getBytes(), filename);

            //将图片名称存到redis
            jedisPool.getResource().sadd(RedisConstants.PIC_UPLOAD_QINIU, filename);

            return new Result(true, MessageConstant.PIC_UPLOAD_SUCCESS, filename);

        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.PIC_UPLOAD_FAIL);
        }
    }

    /**
     * 添加套餐
     */
    @RequestMapping("/add")
    public Result add(@RequestBody Setmeal setmeal, Integer[] checkgroupIds) {

        try {
            //调用业务，添加套餐
            setmealService.add(setmeal, checkgroupIds);
            //添加成功，则将图片名字保存到redis
            jedisPool.getResource().sadd(RedisConstants.PIC_UPLOAD_DB, setmeal.getImg());

            return new Result(true, MessageConstant.ADD_SETMEAL_SUCCESS);

        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ADD_SETMEAL_FAIL);
        }

    }

}
