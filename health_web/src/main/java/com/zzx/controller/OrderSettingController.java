package com.zzx.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zzx.constant.MessageConstant;
import com.zzx.entity.Result;
import com.zzx.pojo.OrderSetting;
import com.zzx.service.OrderSettingService;
import com.zzx.utils.POIUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@RestController
@RequestMapping("/ordersetting")
public class OrderSettingController {

    @Reference
    private OrderSettingService orderSettingService;

    @RequestMapping("/upload")
    public Result upload(@RequestBody MultipartFile excelFile) {
        try {
            //读取EXCEL表格，获取一行行的表格数据
            List<String[]> lists = POIUtils.readExcel(excelFile);
            //转成List<OrderSetting>
            List<OrderSetting> orderSettings = new ArrayList<>();
            for (String[] o : lists) {
                orderSettings.add(new OrderSetting(new Date(o[0]), Integer.parseInt(o[1])));
            }
            //调用业务，保存预约设置
            orderSettingService.add(orderSettings);
            return new Result(true, MessageConstant.UPLOAD_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.UPLOAD_FAIL);
        }
    }

    @RequestMapping(value = "/getOrderSettingByMonth", method = RequestMethod.GET)
    public Result getOrderSettingByMonth(String date) {
        try {
            List<Map<String, Object>> list = orderSettingService.getOrderSettingByMonth(date);
            System.out.println(list);
            return new Result(true, MessageConstant.GET_ORDERSETTING_SUCCESS, list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_ORDERSETTING_FAIL);
        }
    }

    @RequestMapping(value = "/editNumberByDate", method = RequestMethod.POST)
    public Result editNumberByDate(@RequestBody OrderSetting orderSetting) {
        try {
            orderSettingService.editNumberByDate(orderSetting);
            return new Result(true, MessageConstant.ORDERSETTING_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ORDERSETTING_FAIL);
        }
    }

}
