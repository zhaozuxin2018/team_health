package com.zzx.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zzx.constant.MessageConstant;
import com.zzx.constant.RedisConstants;
import com.zzx.entity.PageResult;
import com.zzx.entity.QueryPageBean;
import com.zzx.entity.Result;
import com.zzx.service.CheckGroupService;
import com.zzx.service.MemberService;
import com.zzx.utils.QiniuUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

@RestController
@RequestMapping("/member")
public class memberController {


    /**
     * 引入提供者暴露的接口服务
     */
    @Reference
    private MemberService memberService;

    /**
     * 分页查询
     *
     * @param queryPageBean
     * @return
     */
    @RequestMapping("/findPage")
    private PageResult findPage(@RequestBody QueryPageBean queryPageBean) {
        //会员档案分页
        return memberService.findPage(queryPageBean);
    }

    @RequestMapping("/report")
    private PageResult report(@RequestBody QueryPageBean queryPageBean) {
        //体检状态分页
        return memberService.findStatePage(queryPageBean);
    }

    /**
     * 文件上传
     */
    @RequestMapping("/upload")
    private Result upload(@RequestBody MultipartFile excelFile) {
        try {
            //获取图片名称
            String oriName = excelFile.getOriginalFilename();

            //调用七牛云工具类，上传至七牛云
            QiniuUtils.upload2Qiniu(excelFile.getBytes(), oriName);

            return new Result(true, MessageConstant.UPLOAD_SUCCESS);

        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.UPLOAD_FAIL);
        }
    }


}
