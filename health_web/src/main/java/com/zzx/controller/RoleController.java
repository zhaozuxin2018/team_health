package com.zzx.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zzx.constant.MessageConstant;
import com.zzx.entity.PageResult;
import com.zzx.entity.QueryPageBean;
import com.zzx.entity.Result;
import com.zzx.pojo.CheckItem;
import com.zzx.pojo.Role;
import com.zzx.service.RoleService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/role")
public class RoleController {

    @Reference
    private RoleService roleService;

    /**
     * 获取新增角色回显数据
     *
     * @return
     */
    @RequestMapping("/getBackData")
    public Result getUserName() {
        try {
            Map<String, Object> map = roleService.getBackData();
            return new Result(true, MessageConstant.GET_ROLE_DATA_SUCCESS, map);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_ROLE_DATA_FAIL);
        }
    }

    /**
     * 添加角色数据
     *
     * @param role
     * @param checkitemIdsOfMenu
     * @param checkitemIdsOfPermission
     * @return
     */
    @RequestMapping(value = "/addNewRole")
    public Result addNewRole(@RequestBody Role role, int[] checkitemIdsOfMenu, int[] checkitemIdsOfPermission) {
        try {
            roleService.addNewRole(role, checkitemIdsOfMenu, checkitemIdsOfPermission);
            return new Result(true, MessageConstant.ADD_ROLE_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ADD_ROLE_FAIL);
        }
    }

    /**
     * 分页查询角色数据
     *
     * @return
     */
    @RequestMapping("/findRoleByPage")
    public Result findRoleByPage(@RequestBody QueryPageBean queryPageBean) {
        try {
            PageResult pageResult = roleService.findRoleByPage(queryPageBean);
            return new Result(true, MessageConstant.QUERY_ROLELIST_SUCCESS, pageResult);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_ROLELIST_FAIL);
        }
    }

    /**
     * 编辑数据回显
     *
     * @return
     */
    @RequestMapping(value = "/editRole", params = "id")
    public Result editReturnData(String id) {
        try {
            Map<String, Object> map = roleService.editReturnData(id);
            return new Result(true, MessageConstant.QUERY_ROLELIST_SUCCESS, map);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_ROLELIST_FAIL);
        }
    }

    /**
     * 提交编辑后的角色数据
     *
     * @param role
     * @param checkitemIdsOfMenu
     * @param checkitemIdsOfPermission
     * @return
     */
    @RequestMapping(value = "/commitEditRole")
    public Result commitEditReturnData(@RequestBody Role role, int[] checkitemIdsOfMenu, int[] checkitemIdsOfPermission, String id) {
        try {
            if (1 == role.getId()) {
                return new Result(false, MessageConstant.EDIT_ROLE_ADMIN_FAIL);
            }
            roleService.commitEditReturnData(role, checkitemIdsOfMenu, checkitemIdsOfPermission);
            return new Result(true, MessageConstant.EDIT_ROLE_DATA_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.EDIT_ROLE_DATA_FAIL);
        }
    }

    /**
     * 删除角色数据
     *
     * @param id
     * @return
     */
    @RequestMapping("/deleteRole")
    public Result deleteRole(String id) {
        try {
            if ("1".equals(id)) {
                return new Result(false, MessageConstant.DELETE_ROLE_ADMIN_FAIL);
            }
            roleService.deleteRoleById(id);
            return new Result(true, MessageConstant.DELETE_ROLE_DATA_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.DELETE_ROLE_DATA_FAIL);
        }
    }

    /**
     * 查询所有角色
     */
    @RequestMapping("/findAll")
    public Result findAll() {
        try {
            //调用业务
            List<Role> roleList = roleService.findAll();
            return new Result(true, "查询所有角色成功", roleList);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "查询所有角色失败");
        }

    }


}
