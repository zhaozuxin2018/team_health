package com.zzx.utils;



import java.util.*;

public class AgeComparison {

//    public List<Integer>age(List<Date> dates) {
//       /* long ageTime=0;
//        List<Integer>age = new ArrayList<>();
//        //现在时间
//        Date date = new Date();
//        for (Date date2 : dates) {
//            long time = date.getTime();
//            //会员出生时间
//            Date age2 = date2;
//            long time1 = age2.getTime();
//            //获得会员年龄
//            ageTime = time - time1;
//
//            age.add((int)ageTime);
//        }
//        return age ;*/
//    }

    //由出生日期获得年龄
    public static  int getAge(Date birthDay) throws Exception {
        Calendar cal = Calendar.getInstance();
        if (cal.before(birthDay)) {
            throw new IllegalArgumentException(
                    "The birthDay is before Now.It's unbelievable!");
        }
        int yearNow = cal.get(Calendar.YEAR);
        int monthNow = cal.get(Calendar.MONTH);
        int dayOfMonthNow = cal.get(Calendar.DAY_OF_MONTH);
        cal.setTime(birthDay);

        int yearBirth = cal.get(Calendar.YEAR);
        int monthBirth = cal.get(Calendar.MONTH);
        int dayOfMonthBirth = cal.get(Calendar.DAY_OF_MONTH);

        int age = yearNow - yearBirth;

        if (monthNow <= monthBirth) {
            if (monthNow == monthBirth) {
                if (dayOfMonthNow < dayOfMonthBirth) age--;
            }else{
                age--;
            }
        }
        return age;
    }

    //条件判断
    public static List<Map<String,Object>> AgeJudgment( List<Integer> age) {



        List<Map<String,Object>> list = new ArrayList<>();
        Map<String,Object> map = new HashMap<>();
        Map<String,Object> map2 = new HashMap<>();
        Map<String,Object> map3 = new HashMap<>();
        Map<String,Object> map4 = new HashMap<>();

        int count = 0;//条件1
        int count2 = 0;//条件2
        int count3 = 0;//条件3
        int count4 = 0;//条件4

        String a = "0-18岁之间的用户";
        String a2 = "18-30岁之间的用户";
        String a3 = "30-45岁之间的用户";
        String a4 = "45岁以上的用户";

        for (Integer integer : age) {
        //条件1
            if (integer >= 0 && integer < 18) {
                count++;

            }

            //条件2
            if (integer >= 18 && integer < 30) {
                count2++;

            }

            //条件3
            if (integer >= 30 && integer < 45) {
                count3++;

            }

            //条件4
            if (integer > 45) {
                count4++;
            }

        }
        map.put("value",count);
        map.put("name",a );
        list.add(map);


        map2.put("value",count2);
        map2.put("name",a2);
        list.add(map2);


        map3.put("value",count3);
        map3.put("name",a3 );
        list.add(map3);

        map4.put("value",count4);
        map4.put("name",a4);
        list.add(map4);

        return list;
    }
}
