package com.zzx.dao;

import com.github.pagehelper.Page;
import com.zzx.pojo.Member;

import java.util.Map;

public interface MemberDao {

    /**
     * 根据号码查询
     */
    public Member findByPhoneNumber(String phoneNumber);

    /**
     * 添加会员
     *
     * @param member
     */
    void add(Member member);

    /**
     * 根据会员id查询会员名
     *
     * @param memberId
     * @return
     */
    String findNameById(Integer memberId);

    /**
     * 根据号码查询是否注册会员了
     *
     * @param telephone
     * @return
     */
    int findCountByPhoneNumber(String telephone);

    /**
     * 今日新增数
     *
     * @param reportDate
     * @return
     */
    Long findTodayNewMember(String reportDate);

    /**
     * 总会员数
     *
     * @param
     * @return
     */
    Long findTotalMember();

    /**
     * 根据日期查询新增会员
     *
     * @param thisWeekMonday
     * @return
     */
    Long findNewMemberByDate(String thisWeekMonday);

    /**
     * 会员档案分页
     * @param queryString
     * @return
     */
    Page<Member> findPage(String queryString);

    /**
     * 体检状态分页
     * @param queryString
     * @return
     */
    Page<Map> findStatePage(String queryString);
}
