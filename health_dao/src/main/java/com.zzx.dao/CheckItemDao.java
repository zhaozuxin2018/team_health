package com.zzx.dao;

import com.github.pagehelper.Page;
import com.zzx.pojo.CheckItem;

import java.util.List;

public interface CheckItemDao {
    /**
     * 带条件分页查询
     *
     * @param queryString
     * @return
     */
    Page<CheckItem> findPage(String queryString);

    /**
     * 添加检查项
     *
     * @param checkItem
     */
    void add(CheckItem checkItem);

    /**
     * 先判断该检查项是否和其他检查组有关联
     *
     * @param id
     * @return
     */
    int findCountByCheckItemId(Integer id);

    /**
     * 根据检查项id删除与检查组的关联
     *
     * @param id
     */
    void deleteRelevanceByCheckItemId(Integer id);

    /**
     * 根据id删除检查项
     *
     * @param id
     */
    void deleteById(Integer id);

    /**
     * 根据id查询检查项根据id查询检查项
     *
     * @param id
     * @return
     */
    CheckItem findById(Integer id);

    /**
     * 根据id编辑检查项
     *
     * @param
     */
    void editById(CheckItem checkItem);

    /**
     * 查询所有检查项
     *
     * @return
     */
    List<CheckItem> findAll();
}
