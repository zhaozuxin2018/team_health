package com.zzx.dao;


import java.util.Date;
import java.util.List;
import java.util.Map;

public interface ReportDao {
    /**
     * 获取月份对应的会员数
     *
     * @param monStr
     * @return
     */
    Integer findCountByMonth(String monStr);

    /**
     * 获取套餐的数量和对应的名称
     *
     * @return
     */
    List<Map<String, Object>> getSetmealReport();

    Map<String, Object> getMemberNumber();

    /**
     * 年龄条件
     */
    List<Date> setmealCount();
}
