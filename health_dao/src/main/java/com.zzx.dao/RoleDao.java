package com.zzx.dao;

import com.github.pagehelper.Page;
import com.zzx.pojo.CheckGroup;
import com.zzx.pojo.Menu;
import com.zzx.pojo.Permission;
import com.zzx.pojo.Role;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface RoleDao {
    /**
     * 根据用户id查询role
     *
     * @param id
     * @return
     */
    Set<Role> findByUserId(Integer id);

    /**
     * 查找全部页面数据
     *
     * @return
     */
    List<Menu> findAllMenu();


    /**
     * 查找全部权限数据
     *
     * @return
     */
    List<Permission> findAllPermission();

    /**
     * 新增角色
     *
     * @param role
     */
    void addNewRole(Role role);

    /**
     * 添加角色与页面的关系
     *
     * @param map
     */
    void addRelationshipWithMenu(Map map);

    /**
     * 添加角色与权限的关系
     *
     * @param map
     */
    void addRelationshipWithPermission(Map map);

    /**
     * 分页查询角色数据
     *
     * @param queryString
     * @return
     */
    Page<Role> findPage(String queryString);

    /**
     * 通过角色id查询角色表
     *
     * @param id
     * @return
     */
    Role findRoleById(String id);

    /**
     * 通过角色id查询页面与角色中间表
     *
     * @param id
     * @return
     */
    List<Integer> findRelationshipWithMenu(String id);

    /**
     * 通过角色id查询角色与权限中间表
     *
     * @param id
     * @return
     */
    List<Integer> findRelationshipWithPermission(String id);

    /**
     * 通过角色id更新角色信息
     *
     * @param role
     */
    void updateRole(Role role);

    /**
     * 通过角色id删除t_role_menu数据
     *
     * @param id
     */
    void deleteRoleAndMenu(String id);

    /**
     * 通过角色id删除t_role_permission数据
     *
     * @param id
     */
    void deleteRoleAndPermission(String id);

    /**
     * 通过角色id删除角色信息
     *
     * @param id
     */
    void deleteRoleById(String id);

    List<Role> findAll();
}
