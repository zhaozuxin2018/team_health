package com.zzx.dao;

import com.zzx.pojo.OrderSetting;

import java.util.Date;
import java.util.List;

public interface OrderSettingDao {
    /**
     * 查询预约设置日期是否已经存在
     *
     * @param orderDate
     * @return
     */
    int findCountByOrderDate(Date orderDate);

    /**
     * 根据预约日期更新可预约数
     */
    void editNumberByOrderDate(OrderSetting orderSetting);

    /**
     * 保存预约设置
     *
     * @param orderSetting
     */
    void add(OrderSetting orderSetting);

    /**
     * 根据月份获取预约设置信息
     *
     * @param date
     * @return
     */
    List<OrderSetting> getOrderSettingByMonth(String date);

    /**
     * 根据日期获取预约设置
     *
     * @param orderDate
     * @return
     */
    OrderSetting findByOrderDate(Date orderDate);

    /**
     * 将预约设置的已预约数加一
     */
    void editReservations(Date oDate);

    /**
     * 而对于已经过去的历史数据可以定时来进行清理，
     * 例如每月最后一天凌晨2点执行一次清理任务
     */
    void cleanOrderSettingDao(String orderDate);
}
