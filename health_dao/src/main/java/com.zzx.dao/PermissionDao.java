package com.zzx.dao;

import com.github.pagehelper.Page;
import com.zzx.pojo.CheckItem;
import com.zzx.pojo.Permission;
import com.zzx.pojo.User;

import java.util.Set;

public interface PermissionDao {

    /**
     * 根据用户名查询User
     *
     * @param username
     * @return
     */
    User findUserByUsername(String username);

    /**
     * 根据角色id查询相应的权限
     *
     * @param id
     * @return
     */
    Set<Permission> findByRoleId(Integer id);

    /**
     * 获得分页数据对象
     * @param queryString
     * @return
     */
    Page<Permission> findPage(String queryString);

    void add(Permission permission);

    int findCountByPermissionId(Integer id);

    void deleteById(Integer id);

    void editById(Permission permission);

    Permission findById(Integer id);
}
