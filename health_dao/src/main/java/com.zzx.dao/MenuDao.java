package com.zzx.dao;


import com.github.pagehelper.Page;
import com.zzx.pojo.CheckItem;
import com.zzx.pojo.Menu;

import java.util.List;
import java.util.Map;

/**
 * 菜单持久层接口 
 */
public interface MenuDao {

    /**
     * 查询菜单数据
     *
     * @param map
     * @return 菜单数据
     */
    List<Menu> findMenuInfo(Map map);

    /**
     * 查询子菜单数据
     *
     * @param map 菜单
     * @return 子菜单数据
     */
    List<Menu> findSonMenuInfoByMenuId(Map map);

    /**
     * 获取图标列表
     *
     * @return
     */
    List<String> findIcos();

    /**
     * 获取Pids
     *
     * @return
     */
    List<Map<String, Object>> findPids();

    /**
     * 添加菜单
     *
     * @param menu
     */
    void add(Menu menu);

    /**
     * 一级菜单处理
     *
     * @param parseInt
     * @return
     */
    Long showPathByOne(int parseInt);

    /**
     * 二级菜单处理
     *
     * @param parseInt
     */
    String showPathByTwo(int parseInt, String parentMenuId);

    /**
     * 分页
     * @param queryString
     * @return
     */
    Page<Menu> findPage(String queryString);

    Menu findById(Integer id);

    void editById(Menu menu);

    int findCountByMenuId(Integer id);

    void deleteById(Integer id);
}
