package com.zzx.dao;

import com.github.pagehelper.Page;
import com.zzx.pojo.Role;
import com.zzx.pojo.User;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface UserDao {

    /**
     * 根据用户名查询User
     *
     * @param username
     * @return
     */
    User findUserByUsername(String username);


    Page<User> findPage(String queryString);

    void add(User user);

    void addUserAndRole(Map map);

    User findById(Integer id);

    List<Integer> findRoleIdsByUserId(Integer id);

    void edit(User user);

    void deleteRoleAndUserByUserId(Integer id);

    void deleteById(Integer id);
}
