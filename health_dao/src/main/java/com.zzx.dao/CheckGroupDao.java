package com.zzx.dao;

import com.github.pagehelper.Page;
import com.zzx.pojo.CheckGroup;

import java.util.List;
import java.util.Map;

public interface CheckGroupDao {
    /**
     * 分页查询
     *
     * @param queryString
     * @return
     */
    Page<CheckGroup> findPage(String queryString);

    /**
     * 添加检查组
     *
     * @param checkGroup
     */
    void add(CheckGroup checkGroup);

    /**
     * 添加检查项和检查组的联系
     *
     * @param map
     */
    void addCheckGroupAndCheckItem(Map map);

    /**
     * 查询检查组
     *
     * @param id
     * @return
     */
    CheckGroup findById(Integer id);

    /**
     * 根据检查组id查询与之关联的检查项id们
     *
     * @param id
     * @return
     */
    List<Integer> findCheckItemIdsByCheckGroupId(Integer id);

    /**
     * 修改检查组
     *
     * @param checkGroup
     */
    void edit(CheckGroup checkGroup);


    /**
     * 根据检查组ID删除中间表的关联
     */
    void deleteCheckItemAndCheckGroupByCheckGroupId(Integer id);

    /**
     * 先判断检查组与套餐是否有关联
     *
     * @param id
     * @return
     */
    int findCountSetmealAndCheckGroupByCheckGroupId(Integer id);

    /**
     * 根据Id删除检查组
     *
     * @param id
     */
    void deleteById(Integer id);

    /**
     * 查询所有检查组
     *
     * @return
     */
    List<CheckGroup> findAll();


}
