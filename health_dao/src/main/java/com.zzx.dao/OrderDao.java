package com.zzx.dao;

import com.zzx.pojo.Order;

public interface OrderDao {

    /**
     * 根据预约条件查询预约是否重复
     *
     * @param order
     * @return
     */
    int findCountByOrder(Order order);

    /**
     * 添加预约表的预约记录
     *
     * @param order
     */
    void add(Order order);

    /**
     * 根据id查询预约信息
     *
     * @param id
     * @return
     */
    Order findById(Integer id);

    /**
     * 查询今日预约数
     *
     * @param reportDate
     * @return
     */
    Long findTodayOrderNumber(String reportDate);

    /**
     * 查询今日到诊数
     *
     * @param reportDate
     * @return
     */
    Long findTodayVisitsNumber(String reportDate);

    /** 根据指定日期到今天的预约总数
     * @param thisWeekMonday
     * @return
     */
    Long findOrderNumberByDate(String thisWeekMonday);

    /**
     * 根据指定日期到今天的就诊数
     * @param thisWeekMonday
     * @return
     */
    Long findVisitsNumberByDate(String thisWeekMonday);
}
