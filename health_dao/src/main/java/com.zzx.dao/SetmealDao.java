package com.zzx.dao;

import com.github.pagehelper.Page;
import com.zzx.pojo.Setmeal;

import java.util.List;
import java.util.Map;

public interface SetmealDao {
    /**
     * 分页查询
     *
     * @param queryString
     * @return
     */
    Page<Setmeal> findPage(String queryString);

    /**
     * 添加套餐
     *
     * @param setmeal
     */
    void add(Setmeal setmeal);

    /**
     * 添加套餐和检查组的关联
     *
     * @param map
     */
    void addAssociationCheckGroupAndSetmeal(Map<String, Integer> map);

    /**
     * 获取套餐列表
     *
     * @return
     */
    List<Setmeal> getSetmeal();

    /**
     * 查询封装后的套餐（包含相应的检查组和检查项）
     *
     * @param id
     * @return
     */
    Setmeal findById(Integer id);

    /**
     * 根据套餐Id查询套餐名
     *
     * @param setmealId
     * @return
     */
    String findNameByID(Integer setmealId);

    /**
     * 热门套餐
     *
     * @return
     */
    List<Map<String, Object>> findHotSetmeal();
}
