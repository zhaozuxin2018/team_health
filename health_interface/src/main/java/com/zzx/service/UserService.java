package com.zzx.service;

import com.zzx.entity.PageResult;
import com.zzx.entity.QueryPageBean;
import com.zzx.pojo.Role;
import com.zzx.pojo.User;

import java.util.List;
import java.util.Set;

public interface UserService {


    /**
     * 根据用户名查询User
     *
     * @return
     */
    User findUserByUsername(String username);

    /**
     * //根据用户id查询封装好了roles
     *
     * @param id
     * @return
     */
    Set<Role> findRoleByUserId(Integer id);

    PageResult findPage(QueryPageBean queryPageBean);

    void add(User user, Integer[] roleIds);

    User findById(Integer id);

    List<Integer> findRoleIdsByUserId(Integer id);

    void edit(User user, Integer[] roleIds);

    void delete(Integer id);
}
