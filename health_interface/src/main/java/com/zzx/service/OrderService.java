package com.zzx.service;

import com.zzx.pojo.Order;

import java.util.Map;

public interface OrderService {
    /**
     * 微信预约套餐
     *
     * @param map
     * @return
     */
    Integer submit(Map map) throws Exception;

    /**
     * 根据id查询预约相关信息
     *
     * @param id
     * @return
     */
    Map<String, Object> findById(Integer id);
    
}
