package com.zzx.service;

import com.zzx.entity.PageResult;
import com.zzx.entity.QueryPageBean;
import com.zzx.pojo.Setmeal;

import java.util.List;

public interface SetmealService {

    /**
     * 分页查询
     *
     * @param queryPageBean
     * @return
     */

    PageResult findPage(QueryPageBean queryPageBean);

    /**
     * 添加套餐
     *
     * @param setmeal
     * @param checkgroupIds
     */
    void add(Setmeal setmeal, Integer[] checkgroupIds);

    /**
     * 获取套餐列表
     *
     * @return
     */
    String getSetmeal() throws Exception;

    /**
     * 查询封装后的套餐（包含相应的检查组和检查项）
     *
     * @param id
     * @return
     */
    String findById(String id) throws Exception;


}
