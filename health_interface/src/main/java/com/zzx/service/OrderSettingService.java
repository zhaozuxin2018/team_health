package com.zzx.service;

import com.zzx.pojo.OrderSetting;

import java.util.List;
import java.util.Map;

public interface OrderSettingService {
    /**
     * 保存预约设置
     *
     * @param orderSettings
     */
    void add(List<OrderSetting> orderSettings);

    /**
     * 根据月份获取预约设置信息
     *
     * @param date
     * @return
     */
    List<Map<String, Object>> getOrderSettingByMonth(String date);

    /**
     * 根据日期修改可预约数
     *
     * @param orderSetting
     */
    void editNumberByDate(OrderSetting orderSetting);
}
