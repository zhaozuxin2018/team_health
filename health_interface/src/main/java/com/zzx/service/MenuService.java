package com.zzx.service;

import com.zzx.entity.PageResult;
import com.zzx.entity.QueryPageBean;
import com.zzx.pojo.CheckItem;
import com.zzx.pojo.Menu;

import java.util.List;
import java.util.Map;

/**
 * 菜单服务层 
 */
public interface MenuService {

    /**
     * 查询菜单数据
     *
     * @param username
     * @return 菜单数据
     */
    List<Menu> findMenuInfo(String username);

    /**
     * 获取图标列表
     *
     * @return
     */
    List<Map<String, Object>> findIcos();

    /**
     * 查询父级菜单IDS
     *
     * @return
     */
    List<Map<String, Object>> findPids();


    /**
     * 添加菜单
     *
     * @param menu
     */
    void add(Menu menu);

    /**
     * 根据级别回显相应的路径
     *
     * @param parseInt
     * @return
     */
    String showPath(int parseInt, String parentMenuId);

    /**
     * 分页
     *
     * @param queryPageBean
     * @return
     */
    PageResult findPage(QueryPageBean queryPageBean);

    Menu findById(Integer id);

    void edit(Menu menu);

    void delete(Integer id);
}
