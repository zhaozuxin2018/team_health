package com.zzx.service;

import com.zzx.entity.PageResult;
import com.zzx.entity.QueryPageBean;
import com.zzx.pojo.Member;

public interface MemberService {
    /**
     * 根据号码查询是否注册会员了
     *
     * @param telephone
     * @return
     */
    public int findCountByPhoneNumber(String telephone);

    /**
     * 添加会员
     *
     * @param member
     */
    void add(Member member);

    /**
     * 会员档案分页
     * @param queryPageBean
     * @return
     */
    PageResult findPage(QueryPageBean queryPageBean);

    /**
     * 体检状态分页
     * @param queryPageBean
     * @return
     */
    PageResult findStatePage(QueryPageBean queryPageBean);
}
