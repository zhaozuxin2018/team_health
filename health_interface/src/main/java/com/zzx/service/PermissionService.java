package com.zzx.service;

import com.zzx.entity.PageResult;
import com.zzx.entity.QueryPageBean;
import com.zzx.pojo.CheckItem;
import com.zzx.pojo.Permission;

public interface PermissionService {

    /**
     * 获得分页数据对象
     * @param queryPageBean
     * @return
     */
    PageResult findPage(QueryPageBean queryPageBean);

    /**
     * 添加权限
     * @param permission
     */
    void add(Permission permission);

    void delete(Integer id);

    Permission findById(Integer id);

    void edit(Permission permission);
}
