package com.zzx.service;

import java.util.List;
import java.util.Map;

public interface ReportService {
    /**
     * 查询会员数量
     *
     * @return
     */
    Map<String, Object> getMemberReport();


    /**
     *查询指定月份的会员数量
     * @return
     */
    Map<String, Object> getRangeMemberReport(String[] array);


    /**
     * 会员男女占比
     *
     * @return
     */

    Map<String, Object> getMemberNumber();


    /**
     * 年龄比
     * @return
     */
    Map<String, Object> setmealCount() throws Exception;

    /**
     * 套餐预约占比
     *
     * @return
     */
    Map<String, Object> getSetmealReport();

}
