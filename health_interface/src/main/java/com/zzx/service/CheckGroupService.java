package com.zzx.service;

import com.zzx.entity.PageResult;
import com.zzx.entity.QueryPageBean;
import com.zzx.pojo.CheckGroup;

import java.util.List;

public interface CheckGroupService {
    /**
     * 分页查询
     *
     * @param queryPageBean
     * @return
     */
    PageResult findPage(QueryPageBean queryPageBean);

    /**
     * 添加检查组
     *
     * @param checkGroup
     * @param checkitemIds
     */
    void add(CheckGroup checkGroup, Integer[] checkitemIds);

    /**
     * 查询检查组
     *
     * @param id
     * @return
     */
    CheckGroup findById(Integer id);

    /**
     * 根据检查组id查询与之关联的检查项id们
     *
     * @param id
     * @return
     */
    List<Integer> findCheckItemIdsByCheckGroupId(Integer id);

    /**
     * 编辑检查组
     *
     * @param checkGroup
     * @param checkitemIds
     */
    void edit(CheckGroup checkGroup, Integer[] checkitemIds);

    /**
     * 删除检查组
     *
     * @param id
     */
    void delete(Integer id);

    /**
     * 查询所有检查组
     * @return
     */
    List<CheckGroup> findAll();
}
