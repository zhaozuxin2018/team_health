package com.zzx.service;

import java.util.Map;

public interface BusinessService {
    /**
     * 获取运营数据
     * @return
     */
    Map<String, Object> getBusinessReportData() throws Exception;
}
