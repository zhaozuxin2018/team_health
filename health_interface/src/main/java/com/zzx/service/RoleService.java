package com.zzx.service;

import com.zzx.entity.PageResult;
import com.zzx.entity.QueryPageBean;
import com.zzx.pojo.Role;

import java.util.List;
import java.util.Map;

public interface RoleService {
    /**
     * 获取新增角色回显数据
     *
     * @return
     */
    Map<String, Object> getBackData();

    /**
     * 新增角色
     *
     * @param role
     * @param checkitemIdsOfMenu
     * @param checkitemIdsOfPermission
     */
    void addNewRole(Role role, int[] checkitemIdsOfMenu, int[] checkitemIdsOfPermission);

    /**
     * 分页查询角色数据
     *
     * @param queryPageBean
     */
    PageResult findRoleByPage(QueryPageBean queryPageBean);

    /**
     * 编辑数据回显
     *
     * @param id
     * @return
     */
    Map<String, Object> editReturnData(String id);

    /**
     * 提交编辑后的角色数据
     *
     * @param role
     * @param checkitemIdsOfMenu
     * @param checkitemIdsOfPermission
     */
    void commitEditReturnData(Role role, int[] checkitemIdsOfMenu, int[] checkitemIdsOfPermission);

    /**
     * 通过角色id删除角色数据
     *
     * @param id
     */
    void deleteRoleById(String id);

    List<Role> findAll();
}
