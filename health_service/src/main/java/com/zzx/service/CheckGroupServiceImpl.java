package com.zzx.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zzx.dao.CheckGroupDao;
import com.zzx.entity.PageResult;
import com.zzx.entity.QueryPageBean;
import com.zzx.pojo.CheckGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import redis.clients.jedis.JedisPool;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = CheckGroupService.class)
@Transactional
public class CheckGroupServiceImpl implements CheckGroupService {

    @Autowired
    private CheckGroupDao checkGroupDao;
    @Autowired
    private JedisPool jedisPool;

    /**
     * 分页查询
     *
     * @param queryPageBean
     * @return
     */
    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        PageHelper.startPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());
        Page<CheckGroup> page = checkGroupDao.findPage(queryPageBean.getQueryString());
        return new PageResult(page.getTotal(), page.getResult());
    }

    /**
     * 添加检查组
     *
     * @param checkGroup
     * @param checkitemIds
     */
    @Override
    public void add(CheckGroup checkGroup, Integer[] checkitemIds) {
        //先添加检查组
        checkGroupDao.add(checkGroup);
        //添加检查组和检查项的联系
        addCheckGroupAndCheckItem(checkGroup.getId(), checkitemIds);
       //清空redis 缓存
        jedisPool.getResource().del("setmeal");
    }

    /**
     * 查询检查组
     *
     * @param id
     * @return
     */
    @Override
    public CheckGroup findById(Integer id) {
        return checkGroupDao.findById(id);
    }

    /**
     * 根据检查组id查询与之关联的检查项id们
     *
     * @param id
     * @return
     */
    @Override
    public List<Integer> findCheckItemIdsByCheckGroupId(Integer id) {
        return checkGroupDao.findCheckItemIdsByCheckGroupId(id);
    }

    /**
     * 编辑检查组
     *
     * @param checkGroup
     * @param checkitemIds
     */
    @Override
    public void edit(CheckGroup checkGroup, Integer[] checkitemIds) {
        //先修改检查组
        checkGroupDao.edit(checkGroup);
        //先删除之前的联系
        checkGroupDao.deleteCheckItemAndCheckGroupByCheckGroupId(checkGroup.getId());
        //添加新的联系
        addCheckGroupAndCheckItem(checkGroup.getId(), checkitemIds);
        //清空redis 缓存
        jedisPool.getResource().del("setmeal");
    }

    /**
     * 删除检查组
     *
     * @param id
     */
    @Override
    public void delete(Integer id) {
        //先判断检查组与套餐是否有关联
        int count = checkGroupDao.findCountSetmealAndCheckGroupByCheckGroupId(id);
        if (count > 0) {
            throw new RuntimeException("该检查组与套餐有关联，无法删除");
        }
        //先删除检查组与检查项之间的关联
        checkGroupDao.deleteCheckItemAndCheckGroupByCheckGroupId(id);
        //再删除检查组
        checkGroupDao.deleteById(id);
        //清空redis 缓存
        jedisPool.getResource().del("setmeal");
    }

    /**
     * 查询所有检查组
     *
     * @return
     */
    @Override
    public List<CheckGroup> findAll() {
        return checkGroupDao.findAll();
    }

    /**
     * 添加检查组和检查项的联系
     */
    public void addCheckGroupAndCheckItem(Integer id, Integer[] checkitemIds) {
        for (Integer checkitemId : checkitemIds) {
            Map map = new HashMap();
            map.put("checkgroup_id", id);
            map.put("checkitem_id", checkitemId);
            //调用业务添加联系
            checkGroupDao.addCheckGroupAndCheckItem(map);
        }
    }
}
