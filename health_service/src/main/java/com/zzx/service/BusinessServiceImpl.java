package com.zzx.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.zzx.dao.BusinessDao;
import com.zzx.dao.MemberDao;
import com.zzx.dao.OrderDao;
import com.zzx.dao.SetmealDao;
import com.zzx.pojo.Member;
import com.zzx.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BusinessServiceImpl implements BusinessService {

    @Autowired
    private BusinessDao businessDao;

    @Autowired
    private MemberDao memberDao;

    @Autowired
    private OrderDao orderDao;

    @Autowired
    private SetmealDao setmealDao;


    /**
     * 获取运营数据
     *
     * @return
     */
    @Override
    public Map<String, Object> getBusinessReportData() throws Exception {
        Map<String, Object> map = new HashMap<>();
        //当日
        String reportDate = DateUtils.parseDate2String(new Date());
        //本周周一
        String thisWeekMonday = DateUtils.parseDate2String(DateUtils.getThisWeekMonday());
        //本月第一天
        String firstday4ThisMonth = DateUtils.parseDate2String(DateUtils.getFirstDay4ThisMonth());

        map.put("reportDate", reportDate);
        map.put("todayNewMember", memberDao.findTodayNewMember(reportDate));
        map.put("totalMember", memberDao.findTotalMember());
        map.put("thisWeekNewMember", memberDao.findNewMemberByDate(thisWeekMonday));
        map.put("thisMonthNewMember", memberDao.findNewMemberByDate(firstday4ThisMonth));
        map.put("todayOrderNumber", orderDao.findTodayOrderNumber(reportDate));
        map.put("todayVisitsNumber", orderDao.findTodayVisitsNumber(reportDate));
        map.put("thisWeekOrderNumber", orderDao.findOrderNumberByDate(thisWeekMonday));
        map.put("thisWeekVisitsNumber", orderDao.findVisitsNumberByDate(thisWeekMonday));
        map.put("thisMonthOrderNumber", orderDao.findOrderNumberByDate(firstday4ThisMonth));
        map.put("thisMonthVisitsNumber", orderDao.findVisitsNumberByDate(firstday4ThisMonth));

        List<Map<String, Object>> mapSetmeals = setmealDao.findHotSetmeal();
        map.put("hotSetmeal", mapSetmeals);

        return map;
    }
}
