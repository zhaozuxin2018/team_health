package com.zzx.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zzx.dao.SetmealDao;
import com.zzx.entity.PageResult;
import com.zzx.entity.QueryPageBean;
import com.zzx.pojo.Setmeal;
import org.springframework.beans.factory.annotation.Autowired;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service(interfaceClass = SetmealService.class)
public class SetmealServiceImpl implements SetmealService {

    @Autowired
    private SetmealDao setmealDao;

    //使用redis 工具来缓存数据
    @Autowired
    private JedisPool jedisPool;




    /**
     * 分页查询
     *
     * @param queryPageBean
     * @return
     */
    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        PageHelper.startPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());
        Page<Setmeal> page = setmealDao.findPage(queryPageBean.getQueryString());
        return new PageResult(page.getTotal(), page.getResult());
    }

    /**
     * 添加套餐
     *
     * @param setmeal
     * @param checkgroupIds
     */
    @Override
    public void add(Setmeal setmeal, Integer[] checkgroupIds) {
        //先添加套餐对象
        setmealDao.add(setmeal);
        //如果不为空，就调用方法来添加套餐和检查组的关联
        addCheckGroupIdsBySetmealId(setmeal, checkgroupIds);
    }

    /**
     * 获取套餐列表
     *
     * @return
     */
    @Override
    public String getSetmeal() throws Exception {
        Jedis jedis = jedisPool.getResource();
        String data = jedis.get("setmealList");
        if (data==null){
            //如果从reids 当中数据为空 则从数据库查询返回信息 并保存reids当中
            List<Setmeal> setmealList = setmealDao.getSetmeal();
           // 将数据转为String 格式存入redis
            String jsonString = JSON.toJSONString(setmealList);
            jedis.set("setmealList",jsonString);
            return jsonString ;
        }
//如果redis 不为空的 获取redis 当中的数据返回
      return   jedis.get("setmealList");
              }





    /**
     * 查询封装后的套餐（包含相应的检查组和检查项）
     *
     * @param id
     * @return
     */

    @Override
    public String findById(String id) throws Exception {
        Jedis jedis = jedisPool.getResource();
        String data = jedis.get("setmeal");

       if (data==null){
            //如果从reids 当中数据为空 则从数据库查询返回信息 并保存reids当中
            Setmeal setmeal = setmealDao.findById(Integer.parseInt(id));
            // 将数据转为String 格式存入redis
           String jsonString = JSON.toJSONString(setmeal);
           jedis.set("setmeal",jsonString);
            return jsonString ;
        }
        //如果redis 不为空的 获取redis 当中的数据返回
        return   jedis.get("setmeal");


    }




    /**
     * 添加套餐和检查组的关联
     */
    public void addCheckGroupIdsBySetmealId(Setmeal setmeal, Integer[] checkgroupIds) {
        if (checkgroupIds != null && checkgroupIds.length > 0) {
            for (Integer checkgroupId : checkgroupIds) {
                Map<String, Integer> map = new HashMap<>();
                map.put("setmeal_id", setmeal.getId());
                map.put("checkgroup_id", checkgroupId);
                setmealDao.addAssociationCheckGroupAndSetmeal(map);
            }
        }
    }
}
