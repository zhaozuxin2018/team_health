package com.zzx.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zzx.dao.PermissionDao;
import com.zzx.dao.RoleDao;
import com.zzx.dao.UserDao;
import com.zzx.entity.PageResult;
import com.zzx.entity.QueryPageBean;
import com.zzx.pojo.CheckGroup;
import com.zzx.pojo.Permission;
import com.zzx.pojo.Role;
import com.zzx.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import redis.clients.jedis.JedisPool;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private PermissionDao permissionDao;


    /**
     * 根据用户名查询User
     *
     * @return
     */
    @Override
    public User findUserByUsername(String username) {
        return userDao.findUserByUsername(username);
    }

    /**
     * 根据用户id查询封装好了roles
     *
     * @param id
     * @return
     */
    @Override
    public Set<Role> findRoleByUserId(Integer id) {
        Set<Role> roles = roleDao.findByUserId(id);
        for (Role role : roles) {
            //根据角色id查询相应的权限
            Set<Permission> permissions = permissionDao.findByRoleId(role.getId());
            role.setPermissions(permissions);
        }
        return roles;
    }

    /**
     * 分页查询
     *
     * @param queryPageBean
     * @return
     */
    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        PageHelper.startPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());
        Page<User> page = userDao.findPage(queryPageBean.getQueryString());
        return new PageResult(page.getTotal(), page.getResult());
    }


    /**
     * 添加用户
     *
     * @param
     * @param
     */
    @Override
    public void add(User user, Integer[] roleIds) {
        //先添加用户
        System.out.println(user.getPassword());
        userDao.add(user);
        //添加中间表
        addUserAndRole(user.getId(), roleIds);
    }

    /**
     * 添加检查组和检查项的联系
     */
    public void addUserAndRole(Integer id, Integer[] roleIds) {
        if (roleIds != null && roleIds.length > 0) {
            for (Integer c : roleIds) {
                Map map = new HashMap();
                map.put("user_id", id);
                map.put("role_id", c);
                //调用业务添加联系
                userDao.addUserAndRole(map);
            }
        }
    }

    /**
     * 查询检查组
     *
     * @param id
     * @return
     */
    @Override
    public User findById(Integer id) {
        return userDao.findById(id);
    }

    @Override
    public List<Integer> findRoleIdsByUserId(Integer id) {
        return userDao.findRoleIdsByUserId(id);
    }

    /**
     * 编辑检查组
     *
     * @param
     * @param
     */
    @Override
    public void edit(User user, Integer[] roleIds) {
        //先修改检查组
        userDao.edit(user);
        //先删除之前的联系
        userDao.deleteRoleAndUserByUserId(user.getId());
        //添加新的联系
        addUserAndRole(user.getId(), roleIds);
    }

    /**
     * 删除检查组
     *
     * @param id
     */
    @Override
    public void delete(Integer id) {
        //先删除检查组与检查项之间的关联
        userDao.deleteRoleAndUserByUserId(id);
        //再删除检查组
        userDao.deleteById(id);

    }

}
