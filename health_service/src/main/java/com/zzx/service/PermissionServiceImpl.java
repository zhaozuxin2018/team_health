package com.zzx.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zzx.dao.PermissionDao;
import com.zzx.entity.PageResult;
import com.zzx.entity.QueryPageBean;
import com.zzx.pojo.CheckItem;
import com.zzx.pojo.Permission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Service(interfaceClass = PermissionService.class )
@Transactional
public class PermissionServiceImpl implements PermissionService {

    @Autowired
    private PermissionDao permissionDao;

    /**
     * 获得分页数据对象
     * @param queryPageBean
     * @return
     */
    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        //调用分页插件,获得分页数据
        PageHelper.startPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());
        Page<Permission> page = permissionDao.findPage(queryPageBean.getQueryString());
        return new PageResult(page.getTotal(), page.getResult());
    }

    /**
     * 添加权限
     * @param permission
     */
    @Override
    public void add(Permission permission) {
        permissionDao.add(permission);
    }

    /**
     * 根据id删除权限
     *
     * @param id
     */
    @Override
    public void delete(Integer id) {
        //先判断该检查项是否和其他检查组有关联,有的话还要先删除关联
        int count = permissionDao.findCountByPermissionId(id);
        if (count > 0) {
            //存在关联，抛出异常
//            checkItemDao.deleteRelevanceByCheckItemId(id);
            throw new RuntimeException("该权限与角色有关联，不可删除");
        }
        //不存在关联，直接删除检查项
        permissionDao.deleteById(id);
    }

    /**
     * 根据id查询权限
     * @param id
     * @return
     */
    @Override
    public Permission findById(Integer id) {
        return permissionDao.findById(id);
    }

    /**
     * 编辑权限
     *
     * @param permission
     */
    @Override
    public void edit(Permission permission) {
        //根据id编辑检查项
        permissionDao.editById(permission);
    }
}
