package com.zzx.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.zzx.constant.MessageConstant;
import com.zzx.dao.ReportDao;
import com.zzx.entity.Result;
import com.zzx.utils.AgeComparison;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.*;

@Service(interfaceClass = ReportService.class)
@Transactional
public class ReportServiceImpl implements ReportService {

    @Autowired
    private ReportDao reportDao;

    /**
     * 查询会员每月的数量
     *
     * @return
     */
    @Override
    public Map<String, Object> getMemberReport() {
        Map<String, Object> map = new HashMap<>();
        //获取过去一年的月份列表
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -12);
        //存储月份列表
        List<String> months = new ArrayList<>();
        //存储月份对应会员数的列表
        List<Integer> memberCount = new ArrayList<>();

        for (int i = 1; i <= 12; i++) {
            calendar.add(Calendar.MONTH, 1);
            Date date = calendar.getTime();
            String monStr = new SimpleDateFormat("yyyy.MM").format(date);
            months.add(monStr);
        }

        //获取月份对应的会员数
        for (String month : months) {
            String monStr = month + ".31";
            memberCount.add(reportDao.findCountByMonth(monStr));
        }
        map.put("months", months);
        map.put("memberCount", memberCount);

        return map;
    }

    /**
     * 查询指定月份的会员数量
     * @param array
     * @return
     */
    @Override
    public Map<String, Object> getRangeMemberReport(String[] array) {
        try {
            Map<String, Object> map = new HashMap<>();
            String minDate = array[0];
            String maxDate = array[1];
            //存储月份列表
            List<String> months = getMonthBetween(minDate, maxDate);
            //存储月份对应会员数的列表
            List<Integer> memberCount = new ArrayList<>();
            //获取月份对应的会员数
            for (String month : months) {
                String monStr = month + ".31";
                memberCount.add(reportDao.findCountByMonth(monStr));
            }
            map.put("months", months);
            map.put("memberCount", memberCount);
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 年龄比
     * @return
     */
    @Override
    public  Map<String, Object> setmealCount() throws Exception {
        Map<String, Object> map = new HashMap<>();

        //  获得所有会员的出生日期
        List<Date> listDate= reportDao.setmealCount();
        List<Integer> listAge = new ArrayList<>();
        for (Date date : listDate) {
            //获得所有会员的年龄
            int age = AgeComparison.getAge(date);
            //存入list集合当中
            listAge.add(age);
        }

        //获得键值对
        List<Map<String, Object>> list1 = AgeComparison.AgeJudgment(listAge);
        map.put("setmealCount",list1);
        List<String> list2 = new ArrayList<>();
        for (Map<String, Object> objectMap : list1) {
            list2.add(objectMap.get("name").toString());
        }

        map.put("setmealNames", list2);


        return map;
    }

    /**
     * 会员数量男女的占比
     *
     * @return
     */
    @Override
    public Map<String, Object> getMemberNumber() {
      /*  Map<String, Object> map = new HashMap<>();
        //获取会员人数和会员的性别
      List<Map<String, Object>> list = reportDao.getMemberNumber();
       map.put("number", list);
        //男数
       // map.put("man",9);
        //女数
       // map.put("women",4);
        //获取会员的性别
        List<String> list2 = new ArrayList<>();
        for (Map<String, Object> objectMap : list) {
           if (list!=null && list.size()>0){
               list2.add(objectMap.get("man").toString()+objectMap.get("lady").toString());
           }

        }

        map.put("gender", list2);

        return map;
*/
        //获取会员人数和会员的性别
        Map<String, Object> map = new HashMap<>();
        Map<String, Object> map2 = reportDao.getMemberNumber();
//       map.put("number", map2);
        //获取会员的性别
        List<String> list = new ArrayList<>();
        List<Map<String, Object>> list2 = new ArrayList<>();
        Set<String> strs = map2.keySet();
        for (String s : strs) {
            Map<String, Object> map3 = new HashMap<>();
            list.add(s);
            String count = map2.get(s).toString();
            map3.put("name", s);
            map3.put("value", count);
            list2.add(map3);
        }

        map.put("number", list2);
        map.put("gender", list);

        return map;

    }

    /**
     * 套餐预约占比
     *
     * @return
     */
    @Override
    public Map<String, Object> getSetmealReport() {
        Map<String, Object> map = new HashMap<>();
        //获取套餐的数量和对应的名称
        List<Map<String, Object>> list = reportDao.getSetmealReport();
        map.put("setmealCount", list);
        //获取套餐对应名称
        List<String> list2 = new ArrayList<>();
        for (Map<String, Object> objectMap : list) {
            list2.add(objectMap.get("name").toString());
        }

        map.put("setmealNames", list2);

        return map;
    }

    /**
     * 获得两个日期之间的所有月份
     * @param minDate
     * @param maxDate
     * @return
     * @throws Exception
     */
    public static List<String> getMonthBetween(String minDate, String maxDate) throws Exception {
        ArrayList<String> result = new ArrayList<String>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");//格式化为年月

        Calendar min = Calendar.getInstance();
        Calendar max = Calendar.getInstance();

        min.setTime(sdf.parse(minDate));
        min.set(min.get(Calendar.YEAR), min.get(Calendar.MONTH), 1);

        max.setTime(sdf.parse(maxDate));
        max.set(max.get(Calendar.YEAR), max.get(Calendar.MONTH), 2);

        Calendar curr = min;
        while (curr.before(max)) {
            result.add(sdf.format(curr.getTime()));
            curr.add(Calendar.MONTH, 1);
        }
        min = null;
        max = null;
        curr = null;
        return result;
    }

}
