package com.zzx.service;

import com.alibaba.dubbo.config.annotation.Service;

@Service(interfaceClass = HelloService.class)
public class HelloServiceImpl implements HelloService {


    @Override
    public String sayHello(String name) {

        return "你好，你是" + name;
    }
}
