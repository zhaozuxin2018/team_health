package com.zzx.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.zzx.constant.MessageConstant;
import com.zzx.dao.MemberDao;
import com.zzx.dao.OrderDao;
import com.zzx.dao.OrderSettingDao;
import com.zzx.dao.SetmealDao;
import com.zzx.pojo.Member;
import com.zzx.pojo.Order;
import com.zzx.pojo.OrderSetting;
import com.zzx.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderSettingDao orderSettingDao;

    @Autowired
    private MemberDao memberDao;

    @Autowired
    private OrderDao orderDao;

    @Autowired
    private SetmealDao setmealDao;


    /**
     * 微信预约套餐
     *
     * @param map
     * @return
     */
    @Override
    public Integer submit(Map map) throws Exception {
        Order orderResult = null;
        String name = (String) map.get("name");
        String sex = (String) map.get("sex");
        String telephone = (String) map.get("telephone");
        String idCard = (String) map.get("idCard");
        String orderDate = (String) map.get("orderDate");
        Integer setmealId = Integer.parseInt(map.get("setmealId") + "");
        //先判断预约的日期其实是否可以预约
        Date oDate = DateUtils.parseString2Date(orderDate);
        int count = orderSettingDao.findCountByOrderDate(oDate);
        if (count <= 0) { //预约日期不可预约
            throw new RuntimeException(MessageConstant.SELECTED_DATE_CANNOT_ORDER);
        }
        //再判断预约日期是否已满
        OrderSetting orderSetting = orderSettingDao.findByOrderDate(oDate);
        if (orderSetting.getNumber() <= orderSetting.getReservations()) {
            throw new RuntimeException(MessageConstant.ORDER_FULL);
        } else { //日期可以预约
            //先判断是否是会员了（手机号）
            Member member = memberDao.findByPhoneNumber(telephone);
            if (member != null) {
                //已经是会员了，再判断是否已经预约了套餐，避免重复预约
                Order order = new Order();
                order.setMemberId(member.getId());
                order.setOrderDate(oDate);
                order.setSetmealId(setmealId);
                int count2 = orderDao.findCountByOrder(order);
                if (count2 > 0) {
                    //已经预约了
                    throw new RuntimeException(MessageConstant.HAS_ORDERED);
                }
            } else { //不是会员，就先注册成会员
                member = new Member(name, sex, idCard, telephone, new Date());
                memberDao.add(member);
            }

            orderResult = new Order(member.getId(), oDate, Order.ORDERTYPE_WEIXIN, Order.ORDERSTATUS_NO, setmealId);
            //添加预约表的预约记录
            orderDao.add(orderResult);
            //将预约设置的已预约数加一
            orderSettingDao.editReservations(oDate);

        }

        return orderResult.getId();
    }

    @Override
    public Map<String, Object> findById(Integer id) {
        Map<String, Object> map = new HashMap<>();
        //根据id查询预约信息
        Order order = orderDao.findById(id);
        //根据会员id查询会员名
        String member = memberDao.findNameById(order.getMemberId());
        //根据套餐Id查询套餐名
        String setmeal = setmealDao.findNameByID(order.getSetmealId());
        //封装map
        map.put("member", member);
        map.put("setmeal", setmeal);
        map.put("orderDate", order.getOrderDate());
        map.put("orderType", order.getOrderType());
        return map;
    }
}
