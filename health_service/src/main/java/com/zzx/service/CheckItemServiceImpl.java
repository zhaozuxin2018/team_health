package com.zzx.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zzx.dao.CheckItemDao;
import com.zzx.entity.PageResult;
import com.zzx.entity.QueryPageBean;
import com.zzx.pojo.CheckItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import redis.clients.jedis.JedisPool;

import java.util.List;

@Service(interfaceClass = CheckItemService.class)
@Transactional
public class CheckItemServiceImpl implements CheckItemService {

    @Autowired
    private CheckItemDao checkItemDao;
    @Autowired
    private JedisPool jedisPool;



    /**
     * 获得分页数据对象
     *
     * @param queryPageBean
     * @return
     */
    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        //调用分页插件,获得分页数据
        PageHelper.startPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());
        Page<CheckItem> page = checkItemDao.findPage(queryPageBean.getQueryString());
        return new PageResult(page.getTotal(), page.getResult());
    }

    /**
     * 添加检查项
     *
     * @param checkItem
     */
    @Override
    public void add(CheckItem checkItem) {
        checkItemDao.add(checkItem);
        //清空redis 缓存
        jedisPool.getResource().del("setmeal");
    }

    /**
     * 根据id删除检查项
     *
     * @param id
     */
    @Override
    public void delete(Integer id) {
        //先判断该检查项是否和其他检查组有关联,有的话还要先删除关联
        int count = checkItemDao.findCountByCheckItemId(id);
        if (count > 0) {
            //存在关联，抛出异常
//            checkItemDao.deleteRelevanceByCheckItemId(id);
            throw new RuntimeException("该检查项与检查组有关联，不可删除");
        }
        //不存在关联，直接删除检查项
        checkItemDao.deleteById(id);
        //清空redis 缓存
        jedisPool.getResource().del("setmeal");
    }

    /**
     * 根据id查询检查项
     *
     * @param id
     * @return
     */
    @Override
    public CheckItem findById(Integer id) {
        return checkItemDao.findById(id);
    }

    /**
     * 编辑检查项
     *
     * @param checkItem
     */
    @Override
    public void edit(CheckItem checkItem) {
        //根据id编辑检查项
        checkItemDao.editById(checkItem);
        //清空redis 缓存
        jedisPool.getResource().del("setmeal");
    }

    /**
     * 查询所有检查项
     *
     * @return
     */
    @Override
    public List<CheckItem> findAll() {
        return checkItemDao.findAll();
    }


}
