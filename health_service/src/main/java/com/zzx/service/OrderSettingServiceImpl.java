package com.zzx.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zzx.dao.OrderSettingDao;
import com.zzx.dao.SetmealDao;
import com.zzx.entity.PageResult;
import com.zzx.entity.QueryPageBean;
import com.zzx.pojo.OrderSetting;
import com.zzx.pojo.Setmeal;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = OrderSettingService.class)
public class OrderSettingServiceImpl implements OrderSettingService {

    @Autowired
    private OrderSettingDao orderSettingDao;


    /**
     * 保存预约设置
     *
     * @param orderSettings
     */
    @Override
    public void add(List<OrderSetting> orderSettings) {
        if (orderSettings != null && orderSettings.size() > 0) {
            for (OrderSetting orderSetting : orderSettings) {
                //先查询预约设置日期是否已经存在
                int count = orderSettingDao.findCountByOrderDate(orderSetting.getOrderDate());
                if (count > 0) {
                    //说明已经存在了，就更新
                    orderSettingDao.editNumberByOrderDate(orderSetting);
                } else {
                    //不存在，直接保存
                    orderSettingDao.add(orderSetting);
                }
            }
        }
    }

    /**
     * 根据月份获取预约设置信息
     *
     * @param date
     * @return
     */
    @Override
    public List<Map<String, Object>> getOrderSettingByMonth(String date) {
        System.out.println(date);
        //调用dao，根据月份获取预约设置信息
        List<OrderSetting> list = orderSettingDao.getOrderSettingByMonth(date);
        List<Map<String, Object>> list2 = new ArrayList<>();
        for (OrderSetting orderSetting : list) {
            Map<String, Object> map = new HashMap<>();
            map.put("date", orderSetting.getOrderDate().getDate());
            map.put("number", orderSetting.getNumber());
            map.put("reservations", orderSetting.getReservations());
            list2.add(map);
        }
        System.out.println(list2);
        return list2;
    }

    /**
     * 根据日期修改可预约数
     *
     * @param orderSetting
     */
    @Override
    public void editNumberByDate(OrderSetting orderSetting) {
        int count = orderSettingDao.findCountByOrderDate(orderSetting.getOrderDate());
        if (count > 0) {
            //存在，直接修改
            orderSettingDao.editNumberByOrderDate(orderSetting);
        } else {
            //不存在，直接添加
            orderSettingDao.add(orderSetting);
        }
    }
}
