package com.zzx.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zzx.dao.RoleDao;
import com.zzx.entity.PageResult;
import com.zzx.entity.QueryPageBean;
import com.zzx.pojo.Menu;
import com.zzx.pojo.Permission;
import com.zzx.pojo.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service(interfaceClass = RoleService.class)
@Transactional
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleDao roleDao;

    /**
     * 获取新增角色回显数据
     *
     * @return
     */
    @Override
    public Map<String, Object> getBackData() {

        List<Menu> menus = roleDao.findAllMenu();

        List<Object> tree = new ArrayList<>();
        for (Menu menu : menus) {
            Map<String, Object> parentMap = new HashMap<>();
            String path = menu.getPath();
            // 字符串长度为1则说明一定是父菜单
            if (path.length() == 1) {
                String parentPathString = menu.getPath();
                parentMap.put("id", menu.getId());
                parentMap.put("label", menu.getName());
                List<Map<String, String>> childList = new ArrayList<>();
                for (Menu childMenu : menus) {
                    String childPath = childMenu.getPath();
                    Map<String, String> childMap = new HashMap<>();
                    // 遍历字符串长度大于2的那么一定是子菜单
                    if (childPath.length() != 1) {
                        String childPathString = childPath.split("-")[0].split("/")[1];
                        // 如果和值和父菜单的值相同，那么就是该父菜单下的子菜单
                        if (parentPathString.equals(childPathString)) {
                            childMap.put("id", childMenu.getId().toString());
                            childMap.put("label", childMenu.getName());
                            childList.add(childMap);
                        }
                    }
                }
                parentMap.put("children", childList);
                tree.add(parentMap);
            }
        }
        List<Permission> permissions = roleDao.findAllPermission();
        Map<String, Object> map = new HashMap<>();
        map.put("data", tree);
        map.put("menuData", menus);
        map.put("permissionData", permissions);
        return map;
    }

    /**
     * 新增角色
     *
     * @param role
     * @param checkitemIdsOfMenu
     * @param checkitemIdsOfPermission
     */
    @Override
    public void addNewRole(Role role, int[] checkitemIdsOfMenu, int[] checkitemIdsOfPermission) {
        roleDao.addNewRole(role);
        Integer id = role.getId();
        // 根据id添加两张表的联系
        addRelationshipWithMenu(id, checkitemIdsOfMenu);
        addRelationshipWithPermission(id, checkitemIdsOfPermission);
    }

    /**
     * 分页查询角色数据
     *
     * @param queryPageBean
     */
    @Override
    public PageResult findRoleByPage(QueryPageBean queryPageBean) {
        PageHelper.startPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());
        Page<Role> page = roleDao.findPage(queryPageBean.getQueryString());
        return new PageResult(page.getTotal(), page.getResult());
    }

    /**
     * 编辑数据回显
     *
     * @param id
     * @return
     */
    @Override
    public Map<String, Object> editReturnData(String id) {
        Role role = roleDao.findRoleById(id);
        List<Integer> menuList = roleDao.findRelationshipWithMenu(id);
        List<Integer> permissionList = roleDao.findRelationshipWithPermission(id);
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("formData", role);
        resultMap.put("checkitemIdsOfMenu", menuList);
        resultMap.put("checkitemIdsOfPermission", permissionList);
        return resultMap;
    }

    /**
     * 提交编辑后的角色数据
     *
     * @param role
     * @param checkitemIdsOfMenu
     * @param checkitemIdsOfPermission
     */
    @Override
    public void commitEditReturnData(Role role, int[] checkitemIdsOfMenu, int[] checkitemIdsOfPermission) {
        Integer id = role.getId();
        roleDao.updateRole(role);
        // 通过id删除两张中间表(t_role_menu和t_role_permission)关系
        roleDao.deleteRoleAndMenu(id.toString());
        roleDao.deleteRoleAndPermission(id.toString());
        // 重新增加角色与中间表的关系
        addRelationshipWithMenu(id, checkitemIdsOfMenu);
        addRelationshipWithPermission(id, checkitemIdsOfPermission);
    }

    /**
     * 通过角色id删除角色数据
     *
     * @param id
     */
    @Override
    public void deleteRoleById(String id) {
        // 先删除中间表
        roleDao.deleteRoleAndMenu(id);
        roleDao.deleteRoleAndPermission(id);
        // 再删除角色表
        roleDao.deleteRoleById(id);
    }

    /**
     * 添加角色与页面表关联
     *
     * @param id
     * @param checkitemIds
     */
    public void addRelationshipWithMenu(Integer id, int[] checkitemIds) {
        for (Integer checkitemId : checkitemIds) {
            Map map = new HashMap();
            map.put("role_id", id);
            map.put("menu_id", checkitemId);
            //调用业务添加联系
            roleDao.addRelationshipWithMenu(map);
        }
    }

    /**
     * 添加角色与权限表关联
     *
     * @param id
     * @param checkitemIds
     */
    public void addRelationshipWithPermission(Integer id, int[] checkitemIds) {
        for (Integer checkitemId : checkitemIds) {
            Map map = new HashMap();
            map.put("role_id", id);
            map.put("permission_id", checkitemId);
            //调用业务添加联系
            roleDao.addRelationshipWithPermission(map);
        }
    }

    /**
     * 查询所有检查项
     *
     * @return
     */
    @Override
    public List<Role> findAll() {
        return roleDao.findAll();
    }

}












