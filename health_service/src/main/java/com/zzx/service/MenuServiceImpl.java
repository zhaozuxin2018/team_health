package com.zzx.service;


import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zzx.dao.MenuDao;
import com.zzx.entity.PageResult;
import com.zzx.entity.QueryPageBean;
import com.zzx.pojo.CheckItem;
import com.zzx.pojo.Menu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional
@Service(interfaceClass = MenuService.class)
public class MenuServiceImpl implements MenuService {

    @Autowired
    private MenuDao menuDao;

    /**
     * 查询菜单数据
     *
     * @param username
     * @return
     */
    @Override
    public List<Menu> findMenuInfo(String username) {
        Map map = new HashMap();
        map.put("username", username);
        List<Menu> menuList = menuDao.findMenuInfo(map);
        // 查询菜单数据
        if (menuList != null && menuList.size() > 0) {
            for (Menu menu : menuList) {
                // 封装查询的参数
                Map map2 = new HashMap();
                map2.put("username", username);
                map2.put("menuId", menu.getId());
                List<Menu> sonMenuList = menuDao.findSonMenuInfoByMenuId(map2);
                menu.setChildren(sonMenuList);
            }
        }
        return menuList;
    }

    /**
     * 获取图标列表
     *
     * @return
     */
    @Override
    public List<Map<String, Object>> findIcos() {
        List<String> icoNames = menuDao.findIcos();
        List<Map<String, Object>> list = new ArrayList<>();
        for (String icoName : icoNames) {
            Map map = new HashMap();
            map.put("label", icoName);
            map.put("value", icoName);
            list.add(map);
        }
        return list;
    }

    /**
     * 查询父级菜单IDS
     *
     * @return
     */
    @Override
    public List<Map<String, Object>> findPids() {
        List<Map<String, Object>> pids = menuDao.findPids();
        List<Map<String, Object>> list = new ArrayList<>();
        for (Map<String, Object> pid : pids) {
            Map map = new HashMap();
            map.put("parentName", pid.get("name"));
            map.put("parentId", pid.get("id"));
            list.add(map);
        }
        return list;
    }

    /**
     * 添加菜单
     *
     * @param menu
     */
    @Override
    public void add(Menu menu) {
        menuDao.add(menu);
    }

    /**
     * 根据级别回显相应的路径
     *
     * @param parseInt
     * @return
     */
    @Override
    public String showPath(int parseInt, String parentMenuId) {
        if (1 == parseInt) {
            //一级菜单处理
            Long result = menuDao.showPathByOne(parseInt);
            result++;
            return result.toString();
        } else {
            //二级菜单处理
            String path = menuDao.showPathByTwo(parseInt, parentMenuId);
            String[] arr = path.split("-");
            path = arr[0] + "-" + (Integer.parseInt(arr[1]) + 1);
            return path;
        }
    }

    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        //调用分页插件,获得分页数据
        PageHelper.startPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());
        Page<Menu> page = menuDao.findPage(queryPageBean.getQueryString());
        return new PageResult(page.getTotal(), page.getResult());
    }


    /**
     * 根据id查询检查项
     *
     * @param id
     * @return
     */
    @Override
    public Menu findById(Integer id) {
        return menuDao.findById(id);
    }

    /**
     * 编辑检查项
     *
     * @param menu
     */
    @Override
    public void edit(Menu menu) {
        //根据id编辑检查项
        menuDao.editById(menu);
    }

    /**
     * 根据id删除检查项
     *
     * @param id
     */
    @Override
    public void delete(Integer id) {
        //先判断该菜单和角色是否有关联,有的话还要先删除关联
        int count = menuDao.findCountByMenuId(id);
        if (count > 0) {
            //存在关联，抛出异常
//            checkItemDao.deleteRelevanceByCheckItemId(id);
            throw new RuntimeException("该菜单与角色有关联，不可删除");
        }
        //不存在关联，直接删除检查项
        menuDao.deleteById(id);
    }
}
